ARG ANSIBLE_VERSION=2.7
FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible:$ANSIBLE_VERSION
MAINTAINER Thierry Hardy <thierry.hardy@orange.com>

RUN rm -f /usr/bin/python && \
    ln -s /usr/local/bin/python /usr/bin/python && \
    pip install openstacksdk==0.52.0 python-openstackclient os-client-config && \
    rm -rf ~/.cache/pip /tmp/*

CMD ["ansible", "--version"]
